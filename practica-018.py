# pip install requests
# pip install beautifulsoup4
# python -m pip install --upgrade pip

import requests
from bs4 import BeautifulSoup

url = "https://listado.mercadolibre.com.ar/computadoras#D[A:computadoras]"

headers = {
  'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/102.0.0.0 Safari/537.36',
}

req = requests.get(url, headers=headers)

# req = requests.get(url)

#print(req)
soup = BeautifulSoup(req.content, 'html.parser')
#print(soup)
lista = soup.select(".ui-search-result__wrapper")

for item in lista:
  #print(item)
  print(item.select_one(".ui-search-item__title").get_text())
  print(item.select_one(".price-tag-fraction").get_text())